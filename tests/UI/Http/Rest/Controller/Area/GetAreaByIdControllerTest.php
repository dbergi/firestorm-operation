<?php

declare(strict_types=1);

namespace App\Tests\UI\Http\Rest\Controller\Area;

use App\Tests\Infrastructure\Share\Event\EventCollectorListener;
use App\Tests\UI\Http\Rest\Controller\JsonApiTestCase;
use Ramsey\Uuid\Uuid;
use Symfony\Component\HttpFoundation\Response;

class GetAreaByIdControllerTest extends JsonApiTestCase
{
    /**
     * @test
     *
     * @group e2e
     *
     * @throws \Assert\AssertionFailedException
     */
    public function invalid_input_parameters_should_return_400_status_code(): void
    {
        $this->createArea();

        $this->post('/api/calculateArea', ['uuid' => 'lksdfjglkjhsl', 'natural' => 12]);

        self::assertSame(Response::HTTP_BAD_REQUEST, $this->cli->getResponse()->getStatusCode());

        /** @var EventCollectorListener $eventCollector */
        $eventCollector = $this->cli->getContainer()->get(EventCollectorListener::class);

        $events = $eventCollector->popEvents();

        self::assertCount(1, $events);
    }

    /**
     * @test
     *
     * @group e2e
     *
     * @throws \Assert\AssertionFailedException
     */
    public function valid_input_parameters_should_return_404_status_code_when_not_exist(): void
    {
        $this->createArea();

        $this->post('/api/getAreaById', ["uuid" => Uuid::uuid4()->toString()]);

        self::assertSame(Response::HTTP_NOT_FOUND, $this->cli->getResponse()->getStatusCode());

        /** @var EventCollectorListener $eventCollector */
        $eventCollector = $this->cli->getContainer()->get(EventCollectorListener::class);

        $events = $eventCollector->popEvents();

        self::assertCount(1, $events);
    }

    /**
     * @test
     *
     * @group e2e
     *
     * @throws \Assert\AssertionFailedException
     */
    public function valid_input_parameters_should_return_201_status_code_when_exist(): void
    {
        $this->post('/api/calculateArea', ['uuid' => Uuid::uuid4()->toString(), 'divider' => 6]);

        self::assertSame(Response::HTTP_CREATED, $this->cli->getResponse()->getStatusCode());

        /** @var EventCollectorListener $eventCollector */
        $eventCollector = $this->cli->getContainer()->get(EventCollectorListener::class);

        $events = $eventCollector->popEvents();

        self::assertCount(1, $events);
    }
}
