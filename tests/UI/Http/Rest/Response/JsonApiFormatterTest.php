<?php

declare(strict_types=1);

namespace App\Tests\UI\Http\Rest\Response;

use App\Application\Query\Collection;
use App\Application\Query\Item;
use App\Domain\Area\Area;
use App\Domain\Shared\ValueObject\DateTime;
use App\Infrastructure\Area\Query\Projections\AreaView;
use App\UI\Http\Rest\Response\JsonApiFormatter;
use PHPUnit\Framework\TestCase;
use Ramsey\Uuid\Uuid;
use Ramsey\Uuid\UuidInterface;

class JsonApiFormatterTest extends TestCase
{
    /**
     * @test
     *
     * @group unit
     *
     * @throws \Exception
     * @throws \Assert\AssertionFailedException
     */
    public function format_collection(): void
    {
        $areas = [
            self::createAreaView(Uuid::uuid4(), 11),
            self::createAreaView(Uuid::uuid4(), 12),
        ];

        $response = JsonApiFormatter::collection(new Collection(1, 10, \count($areas), $areas));

        self::assertArrayHasKey('data', $response);
        self::assertArrayHasKey('meta', $response);
        self::assertArrayHasKey('total', $response['meta']);
        self::assertArrayHasKey('page', $response['meta']);
        self::assertArrayHasKey('size', $response['meta']);
        self::assertCount(2, $response['data']);
    }

    /**
     * @test
     *
     * @group unit
     *
     * @throws \Assert\AssertionFailedException
     * @throws \Exception
     */
    public function format_one_output(): void
    {
        $areaView = self::createAreaView(Uuid::uuid4(), 11);

        $response = JsonApiFormatter::one(new Item($areaView));

        self::assertArrayHasKey('data', $response);
        self::assertSame('AreaView', $response['data']['type']);
        self::assertCount(4, $response['data']['attributes']);
    }

    /**
     * @throws \App\Domain\Shared\Exception\DateTimeException
     * @throws \Assert\AssertionFailedException
     */
    private static function createAreaView(UuidInterface $uuid, int $natural): AreaView
    {
        $view = AreaView::deserialize([
            'uuid'        => $uuid->toString(),
            'natural' => $natural,
            'created_at' => DateTime::now()->toString(),
            'updated_at' => DateTime::now()->toString(),
        ]);

        return $view;
    }
}
